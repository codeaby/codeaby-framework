import React, { useEffect, useState } from 'react'

import { Authentication } from 'codeaby-framework'

const App = () => {
  const [header, setHeader] = useState('')

  const getHeader = async () => {
    const h = await Authentication.getAuthorizationHeader()
    setHeader(h)
  }

  useEffect(() => {
    getHeader()
  }, [])

  return <div>{header}</div>
}

export default App
