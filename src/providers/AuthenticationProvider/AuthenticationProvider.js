import React, { useEffect, useState } from 'react';
import { authState } from '../../services/Firebase/Authentication';

export const AuthenticationContext = React.createContext();

const AuthenticationProvider = ({ children }) => {
  const [auth, setAuth] = useState({ loaded: false, loggedIn: false, user: null });

  useEffect(() => {
    authState(async (user) => {
      setAuth({ loaded: true, loggedIn: Boolean(user), user });
    });
  }, []);

  return <AuthenticationContext.Provider value={auth}>{children}</AuthenticationContext.Provider>;
};

export default AuthenticationProvider;
