import firebase from './Firebase'
import 'firebase/compat/firestore'

const db = firebase.firestore()

export const readDocumentUpdates = (collection, id, callback, error) =>
  db.collection(collection).doc(id).onSnapshot(callback, error)
