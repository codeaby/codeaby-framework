import firebase from './Firebase'
import 'firebase/compat/auth'

const auth = firebase.auth()
const GoogleAuthProvider = new firebase.auth.GoogleAuthProvider()

export const authState = (callback) => auth.onAuthStateChanged(callback)

export const loginWithGoogle = async () =>
  auth.signInWithPopup(GoogleAuthProvider)

export const logout = async () => auth.signOut()

export const getAuthorizationHeader = async () => ({
  Authorization: `Bearer ${await auth.currentUser.getIdToken()}`
})

export default auth
