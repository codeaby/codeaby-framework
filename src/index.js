// Components
export { default as ApplicationBar } from './components/ApplicationBar/ApplicationBar'
export { default as UserMenu } from './components/ApplicationBar/UserMenu/UserMenu'
export { default as Loading } from './components/Loading/Loading'
export { default as LoginScreen } from './components/LoginScreen/LoginScreen'
export { default as MainLayout } from './components/MainLayout/MainLayout'

// Hooks
export { default as useAuth } from './hooks/useAuth/useAuth'
export { default as useLogin } from './hooks/useLogin/useLogin'
export { default as useMenu } from './hooks/useMenu/useMenu'

// Providers
export {
  default as AuthenticationProvider,
  AuthenticationContext
} from './providers/AuthenticationProvider/AuthenticationProvider'
export { default as MainProviders } from './providers/MainProviders/MainProviders'

// Routing
export { default as PrivateRoute } from './routing/PrivateRoute/PrivateRoute'

// Services
export { default as Firebase } from './services/Firebase/Firebase'

import { readDocumentUpdates } from './services/Firebase/Firestore'
const Firestore = { readDocumentUpdates }
export { Firestore }

import {
  authState,
  loginWithGoogle,
  logout,
  getAuthorizationHeader
} from './services/Firebase/Authentication'
const Authentication = {
  authState,
  loginWithGoogle,
  logout,
  getAuthorizationHeader
}
export { Authentication }
