import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import useAuth from '../../hooks/useAuth/useAuth';

/**
 * Route that is restricted to authenticated users.
 * Redirects to /login when not authenticated.
 */
const PrivateRoute = ({ path, exact, children }) => {
  const { loggedIn } = useAuth();

  return (
    <Route path={path} exact={exact}>
      {loggedIn ? children : <Redirect to="/login" />}
    </Route>
  );
};

export default PrivateRoute;
