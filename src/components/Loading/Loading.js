import { Box, CircularProgress, styled } from '@material-ui/core';
import React from 'react';

const LoadingWrapper = styled(({ fullScreen, ...other }) => <Box {...other}></Box>)(
  ({ theme, fullScreen }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2),
    height: fullScreen ? '100vh' : 'auto',
  })
);

const Loading = ({ fullScreen = false }) => {
  return (
    <LoadingWrapper fullScreen={fullScreen}>
      <CircularProgress />
    </LoadingWrapper>
  );
};

export default Loading;
