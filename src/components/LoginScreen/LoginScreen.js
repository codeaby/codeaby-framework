import { Container, Typography } from '@material-ui/core';
import React from 'react';
import useLogin from '../../hooks/useLogin/useLogin';
import { AbbyAvatar, LoginContainer, LoginWithGoogleButton } from './LoginScreenElements';

const LoginScreen = () => {
  const login = useLogin();

  return (
    <Container maxWidth="xs">
      <LoginContainer>
        <AbbyAvatar src="abby.png" />
        <Typography variant="h5">Login</Typography>
        <LoginWithGoogleButton onClick={login}>Login con Google</LoginWithGoogleButton>
        <Typography variant="body2">Copyright © Codeaby 2021.</Typography>
      </LoginContainer>
    </Container>
  );
};

export default LoginScreen;
