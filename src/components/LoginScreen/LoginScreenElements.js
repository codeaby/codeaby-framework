import React from 'react'
import { Avatar, Box, Button, styled } from '@material-ui/core'

export const LoginContainer = styled(Box)(({ theme }) => ({
  marginTop: theme.spacing(8),
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column'
}))

export const AbbyAvatar = styled(Avatar)(({ theme }) => ({
  margin: theme.spacing(1),
  width: theme.spacing(8),
  height: theme.spacing(8),
  backgroundColor: 'grey'
}))

export const LoginWithGoogleButton = styled(({ ...other }) => (
  <Button fullWidth variant='contained' color='primary' {...other}></Button>
))(({ theme }) => ({
  margin: theme.spacing(8, 0)
}))
