import { Avatar, styled } from '@material-ui/core';

export const MenuAvatar = styled(Avatar)(({ theme }) => ({
  width: theme.spacing(4),
  height: theme.spacing(4),
}));
