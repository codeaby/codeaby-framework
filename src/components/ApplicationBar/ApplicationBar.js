import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core'
import { ArrowBack, Menu } from '@material-ui/icons'
import React, { Fragment } from 'react'
import { useHistory } from 'react-router-dom'
import useAuth from '../../hooks/useAuth/useAuth'
import useMenu from '../../hooks/useMenu/useMenu'
import { logout } from '../../services/Firebase/Authentication'
import { MenuAvatar } from './ApplicationBarElements'
import UserMenu from './UserMenu/UserMenu'

const ApplicationBar = ({ title, backButtonLink, publicScreen = false }) => {
  const { user } = useAuth()
  const [menuAnchorEl, openMenu, closeMenu] = useMenu()
  const history = useHistory()

  return (
    <Fragment>
      <AppBar position='static'>
        <Toolbar>
          <IconButton
            edge='start'
            color='inherit'
            aria-label='menu'
            onClick={() => backButtonLink && history.push(backButtonLink)}
          >
            {backButtonLink ? <ArrowBack /> : <Menu />}
          </IconButton>
          <Typography variant='h6' style={{ flexGrow: 1 }}>
            {title}
          </Typography>
          {!publicScreen && (
            <IconButton
              aria-controls='user-menu'
              aria-haspopup='true'
              edge='end'
              color='inherit'
              onClick={openMenu}
            >
              <MenuAvatar src={user.photoURL} />
            </IconButton>
          )}
        </Toolbar>
      </AppBar>
      {!publicScreen && (
        <UserMenu
          id='user-menu'
          menuAnchorEl={menuAnchorEl}
          closeUserMenu={closeMenu}
          userName={user.displayName}
          userEmail={user.email}
          userAvatar={user.photoURL}
          logout={logout}
        />
      )}
    </Fragment>
  )
}

export default ApplicationBar
