import { Container, styled } from '@material-ui/core'
import React, { Fragment } from 'react'
import ApplicationBar from '../ApplicationBar/ApplicationBar'
import Loading from '../Loading/Loading'

const MainContainer = styled(Container)(({ theme }) => ({
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(13)
}))

const MainLayout = ({
  children,
  title,
  backButtonLink,
  loading = false,
  publicScreen = false
}) => {
  return (
    <Fragment>
      <ApplicationBar
        title={title}
        backButtonLink={backButtonLink}
        publicScreen={publicScreen}
      />
      <MainContainer maxWidth='xs'>
        {loading ? <Loading /> : children}
      </MainContainer>
    </Fragment>
  )
}

export default MainLayout
