import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { loginWithGoogle } from '../../services/Firebase/Authentication';
import useAuth from '../useAuth/useAuth';

/**
 * Hook that exposes the login method and redirects users to / when logged in.
 */
const useLogin = () => {
  const history = useHistory();
  const { loggedIn } = useAuth();

  const login = async () => {
    const authentication = await loginWithGoogle();
    if (authentication) {
      history.push('/');
    }
    // TODO: Show error message somewhere
  };

  useEffect(() => {
    if (loggedIn) history.push('/');
  }, [loggedIn, history]);

  return login;
};

export default useLogin;
