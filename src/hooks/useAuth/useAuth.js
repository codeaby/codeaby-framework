import { useContext } from 'react';
import { AuthenticationContext } from '../../providers/AuthenticationProvider/AuthenticationProvider';

/**
 * Hook that exposes access to the AuthenticationContext value:
 * { loaded, loggedIn, user }
 */
const useAuth = () => useContext(AuthenticationContext);

export default useAuth;
