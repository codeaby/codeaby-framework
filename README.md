# codeaby-framework

> A component library for all our ReactJs projects

[![NPM](https://img.shields.io/npm/v/codeaby-framework.svg)](https://www.npmjs.com/package/codeaby-framework) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save codeaby-framework
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'codeaby-framework'
import 'codeaby-framework/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
